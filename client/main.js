Accounts.ui.config({
	passwordSignupFields: 'USERNAME_AND_EMAIL'
});

Comments.ui.config({
	template: 'bootstrap' // or ionic, semantic-ui
});

/////
// template helpers
/////

// helper function that returns all available websites
Template.website_list.helpers({
	websites: function() {
		return Websites.find({}, {
			sort: {
				uprating: -1
			}
		})
	}
});

Template.website_details.helpers({
	getUser: function(user_id) {
		var user = Meteor.users.findOne({
			_id: user_id
		});
		if (user) {
			return user.username;
		} else {
			return "System";
		}
	}
})

Template.website_item.helpers({
	getUser: function(user_id) {
		var user = Meteor.users.findOne({
			_id: user_id
		});
		if (user) {
			return user.username;
		} else {
			return "System";
		}
	}
})


/////
// template events
/////

Template.navbar.events({
	"click .js-toggle-website-form": function(event) {
		$('#website_form').modal('show');
	}
})

Template.website_item.events({
	"click .js-upvote": function(event) {
		if (Meteor.user()) {
			var website_id = this._id;
			var rating = Websites.findOne({
				_id: website_id
			})
			var newrating = rating.uprating;
			newrating = newrating + 1;
			Websites.update({
				_id: website_id
			}, {
				$set: {
					uprating: newrating
				}
			});

			var tags = (this.title).split(" ");
			var recco_tags = [];
			if (Session.get("recco_tags")) {
				recco_tags = Session.get("recco_tags");
				recco_tags = recco_tags.concat(tags);
				Session.set("recco_tags", recco_tags);
			} else {
				Session.set("recco_tags", tags);
			}

		} else {
			alert("You need to log in!");
		}

		return false; // prevent the button from reloading the page
	},
	"click .js-downvote": function(event) {
		if (Meteor.user()) {
			var website_id = this._id;
			var rating = Websites.findOne({
				_id: website_id
			})
			var newrating = rating.downrating;
			newrating = newrating - 1;
			Websites.update({
				_id: website_id
			}, {
				$set: {
					downrating: newrating
				}
			});

		} else {
			alert("You need to log in!");
		}
		return false; // prevent the button from reloading the page
	}
})

Template.website_form.events({
	"focusout #url": function(event) {
		var url = event.target.value;
		extractMeta(url, function(err, response) {
			$('#title').val(response.title);
			$('#description').val(response.description);
		});
		return false; // stop the form submit from reloading the page
	},
	"submit .js-save-website-form": function(event) {
		if (Meteor.user()) {
			var url = event.target.url.value;
			var title = event.target.title.value;
			var description = event.target.description.value;

			Websites.insert({
				title: title,
				url: url,
				description: description,
				createdOn: new Date(),
				createdBy: Meteor.user()._id,
				uprating: 0,
				downrating: 0
			});

			alert("Site Added!");
			$('#website_form').modal('hide')
			$('#title').val("");
			$('#description').val("");
			$('#url').val("");
		} else {
			alert("You need to log in!");
		}
		return false; // stop the form submit from reloading the page
	}
});

Template.searchBox.helpers({
	websiteIndex: () => websiteIndex
});

Template.registerHelper("prettifyDate", function(timestamp) {
	return moment(new Date(timestamp)).fromNow();
});

Template.reccos_list.helpers({
	websites: function() {
		var reccos_list = [];
		reccos_list = Session.get("recco_tags");
		if (reccos_list) {
			var regex_reccos_list = new RegExp(reccos_list.join("|"), 'gi');
			return Websites.find({
				title: {
					$regex: regex_reccos_list
				}
			}, {
				sort: {
					uprating: -1,
					createdOn: -1
				}
			})
		}
	}
});
