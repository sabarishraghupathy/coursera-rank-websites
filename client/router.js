Router.configure({
	layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function() {
	this.render('navbar', {
		to: "navbar"
	});
	this.render('website_form', {
		to: "website_form"
	});
	this.render('website_list', {
		to: "website_list"
	});
});

Router.route('/website_details/:_id', function() {
	this.render('navbar', {
		to: "navbar"
	});
	this.render('website_details', {
		to: "website_list",
		data: function() {
			return Websites.findOne({
				_id: this.params._id
			});
		}
	});
});

Router.route('/reccos', function() {
	this.render('navbar', {
		to: "navbar"
	});
	this.render('reccos_list', {
		to: "website_list"
	});
});
