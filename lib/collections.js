Websites = new Mongo.Collection("websites");

Websites.allow({
	insert: function(userId, doc) {
		if (Meteor.user()) {
			return true;
		} else {
			return false;
		}
	},
	update: function(userId, doc) {
		if (Meteor.user()) {
			return true;
		} else {
			return false;
		}
	}
})

// On Client and Server

websiteIndex = new EasySearch.Index({
	collection: Websites,
	fields: ['title', 'description'],
	defaultSearchOptions: {
		limit: 100
	},
	engine: new EasySearch.Minimongo({
		sort: function() {
			return {
				uprating: -1
			};
		}
	})
});
